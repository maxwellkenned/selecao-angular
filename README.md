# SelecaoAngular

## Uma simples projeto de front para acessar a [API](https://gitlab.com/maxwellkenned/selecao-java)

## Requisito

- [node](https://www.docker.com/)
- [API](https://gitlab.com/maxwellkenned/selecao-java)

## Rodando o projeto

`git clone https://gitlab.com/maxwellkenned/selecao-angular.git`

`cd  selecao-angular`

`npm i`

`npm run start`


O projeto rodará em http://localhost:4200
