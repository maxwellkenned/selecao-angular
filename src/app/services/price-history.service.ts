import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { API } from '../config/api';
import { User } from '../models/User';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PriceHistoryService {
  constructor(private http: HttpClient) {}

  public getAll(page): Observable<any> {
    const url = `${API}/pricehistories?page=${page}`;

    return this.http
      .get(url, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }

  public avgPerCity(city: string): Observable<any> {
    const url = `${API}/pricehistories/avg/city?city=${encodeURIComponent(
      city,
    )}`;

    return this.http
      .get(url, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }

  public priceHistoryPerRegion(
    region: string,
    page: number = 1,
  ): Observable<any> {
    const url = `${API}/pricehistories/region?region=${encodeURIComponent(
      region,
    )}&page=${page}`;

    return this.http
      .get(url, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }

  public priceHistoryPerDistributor(
    distributor: string,
    page: number = 1,
  ): Observable<any> {
    const url = `${API}/pricehistories/distributor?distributor=${encodeURIComponent(
      distributor,
    )}&page=${page}`;

    return this.http
      .get(url, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }

  public priceHistoryPerCollectionDate(
    collectionDate: string,
    page: number = 1,
  ): Observable<any> {
    const url = `${API}/pricehistories/collectiondate?collection_date=${collectionDate}&page=${page}`;

    return this.http
      .get(url, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }

  public allAvgPerCity(page: number = 1): Observable<any> {
    const url = `${API}/pricehistories/cities/avg?page=${page}`;

    return this.http
      .get(url, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }

  public allAvgPerProvider(page: number = 1): Observable<any> {
    const url = `${API}/pricehistories/providers/avg?page=${page}`;

    return this.http
      .get(url, { headers: { 'Content-Type': 'application/json' } })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }
}
