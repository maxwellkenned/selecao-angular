import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { API } from '../config/api';
import { User } from '../models/User';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  constructor(private http: HttpClient) {}

  public login(user: User): Observable<any> {
    return this.http
      .post(`${API}/sessions`, JSON.stringify(user), {
        headers: { 'Content-Type': 'application/json', skipAuth: 'true' },
      })
      .pipe(
        map((data: any) => {
          const user = JSON.stringify(data.user);
          const token = `Bearer ${data.token}`;

          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('token', token);
        }),
        catchError(err => throwError(err.error)),
      );
  }

  public logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
  }
}
