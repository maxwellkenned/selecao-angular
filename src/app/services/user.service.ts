import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { API } from '../config/api';
import { User } from '../models/User';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  public createUser(user: User): Observable<any> {
    return this.http
      .post(`${API}/users`, JSON.stringify(user), {
        headers: { 'Content-Type': 'application/json', skipAuth: 'true' },
      })
      .pipe(
        map(data => data),
        catchError(err => throwError(err.error)),
      );
  }
}
