import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (req.headers.get('skipAuth')) {
      return next.handle(req);
    }

    const token = localStorage.getItem('token');

    if (!token) {
      this.router.navigate(['login']);
      return;
    }

    const dupReq = req.clone({
      headers: req.headers.set('Authorization', localStorage.getItem('token')),
    });

    return next.handle(dupReq);
  }
}
