import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { API } from '../config/api';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UploadService {
  constructor(private http: HttpClient) {}

  public uploadCsv(file): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    return this.http.post(`${API}/import`, formData).pipe(
      map(data => data),
      catchError(err => throwError(err.error)),
    );
  }
}
