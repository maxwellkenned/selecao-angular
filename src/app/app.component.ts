import { Component } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import Swal from 'sweetalert2';
import { successConfig } from './config/swalConfig';

@Component({
  selector: 'app-root',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  constructor(private socket: Socket) {
    this.socket.on('upload', () => {
      const loading = document.getElementById('loading');
      loading.style.display = 'none';

      Swal.fire({
        ...successConfig,
        position: 'bottom-end',
        timer: 0,
        text:
          'A importação do arquivo terminou. Voce pode consultar os dados agora',
      });
    });
  }
}
