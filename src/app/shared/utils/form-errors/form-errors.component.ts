import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-errors',
  templateUrl: './form-errors.component.html',
  styleUrls: ['./form-errors.component.scss'],
})
export class FormErrorsComponent implements OnInit {
  @Input() formBuilder: FormGroup;
  @Input() friendlyName: Object;

  constructor() {}

  ngOnInit() {}

  errorControls() {
    let controls = Object.keys(this.formBuilder.controls).map(control => ({
      name: this.getFriendlyName(control),
      formControl: this.formBuilder.controls[control],
    }));

    controls = controls
      .map(control => ({ ...control, errors: control.formControl.errors }))
      .filter(
        control =>
          control.formControl.errors &&
          (control.formControl.dirty || control.formControl.touched),
      );

    return this.formBuilder.invalid ? controls : [];
  }

  private getFriendlyName(name: string): string {
    return (this.friendlyName && this.friendlyName[name]) || name;
  }
}
