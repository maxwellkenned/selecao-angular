import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormErrorsComponent } from './utils/form-errors/form-errors.component';
import { LoadingComponent } from './loading/loading.component';

@NgModule({
  declarations: [FormErrorsComponent, LoadingComponent],
  imports: [CommonModule],
  exports: [FormErrorsComponent, LoadingComponent]
})
export class SharedModule {}
