import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {
  map,
  debounceTime,
  distinctUntilChanged,
  filter,
} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { errorConfig } from 'src/app/config/swalConfig';

import { PriceHistoryService } from 'src/app/services/price-history.service';

@Component({
  selector: 'app-avg',
  templateUrl: './avg.component.html',
  styleUrls: ['./avg.component.scss'],
})
export class AvgComponent implements OnInit {
  loading: boolean;
  searchTerm = new Subject<string>();
  result: Array<Object>;

  constructor(private priceHistoryService: PriceHistoryService) {
    this.searchTerm
      .pipe(
        map((e: any) => String(e.target.value).toLocaleUpperCase()),
        debounceTime(800),
        distinctUntilChanged(),
        filter(term => term.length > 0),
      )
      .subscribe(term => {
        this.loading = true;
        this.search(term);
      });
  }

  ngOnInit() {}

  search(term: string) {
    this.priceHistoryService.avgPerCity(term).subscribe(
      data => {
        this.result = data;
      },
      error => {
        const msg = (error && error.error) || 'Ocorreu um erro!';
        Swal.fire({ ...errorConfig, text: msg });
        this.loading = false;
      },
      () => (this.loading = false),
    );
  }
}
