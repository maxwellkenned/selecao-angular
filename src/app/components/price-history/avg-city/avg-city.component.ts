import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { errorConfig } from 'src/app/config/swalConfig';

import { PriceHistoryService } from 'src/app/services/price-history.service';
import { paginate } from 'src/app/shared/utils/paginate';

@Component({
  selector: 'app-avg-city',
  templateUrl: './avg-city.component.html',
  styleUrls: ['./avg-city.component.scss'],
})
export class AvgCityComponent implements OnInit {
  result: Array<Object>;
  page: number;
  maxPages: number;

  constructor(private priceHistoryService: PriceHistoryService) {}

  ngOnInit() {
    this.getAll();
  }

  getAll(page: number = 1) {
    this.priceHistoryService.allAvgPerCity(page).subscribe(
      data => {
        this.result = data.result;
        this.page = Number(data.page);
        this.maxPages = data.maxPages;
      },
      error => {
        const msg = (error && error.error) || 'Ocorreu um erro!';
        Swal.fire({ ...errorConfig, text: msg });
      },
    );
  }

  getPage(page) {
    this.resolver(page);
  }

  previous() {
    this.resolver(this.page - 1);
  }

  next() {
    this.resolver(this.page + 1);
  }

  last() {
    this.resolver(this.maxPages);
  }

  first() {
    this.resolver(1);
  }

  getArrNumberPages() {
    const pagination: any = paginate(this.maxPages, this.page);

    return pagination.pages;
  }

  resolver(page) {
    return this.getAll(page);
  }
}
