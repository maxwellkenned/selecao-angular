import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {
  map,
  debounceTime,
  distinctUntilChanged,
  filter,
} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { errorConfig } from 'src/app/config/swalConfig';

import { PriceHistoryService } from 'src/app/services/price-history.service';
import { paginate } from 'src/app/shared/utils/paginate';

@Component({
  selector: 'app-distributor',
  templateUrl: './distributor.component.html',
  styleUrls: ['./distributor.component.scss'],
})
export class DistributorComponent implements OnInit {
  loading: boolean;
  searchTerm = new Subject<string>();
  searchString: string;
  result: Array<Object>;
  page: number;
  maxPages: number;

  constructor(private priceHistoryService: PriceHistoryService) {
    this.searchTerm
      .pipe(
        map((e: any) =>
          String(e.target.value)
            .trim()
            .toLocaleUpperCase(),
        ),
        debounceTime(800),
        distinctUntilChanged(),
        filter(term => term.length > 0),
      )
      .subscribe(term => {
        this.loading = true;
        this.searchString = term;
        this.search(term);
      });
  }

  ngOnInit() {
    this.getAll();
  }

  getAll(page: number = 1) {
    this.priceHistoryService.getAll(page).subscribe(
      data => {
        this.result = data.result;
        this.page = Number(data.page);
        this.maxPages = data.maxPages;
      },
      error => {
        const msg = (error && error.error) || 'Ocorreu um erro!';
        Swal.fire({ ...errorConfig, text: msg });
        this.loading = false;
      },
      () => (this.loading = false),
    );
  }

  search(term: string, page: number = 1) {
    this.priceHistoryService.priceHistoryPerDistributor(term, page).subscribe(
      data => {
        this.result = data.result;
        this.page = Number(data.page);
        this.maxPages = data.maxPages;
      },
      error => {
        const msg = (error && error.error) || 'Ocorreu um erro!';
        Swal.fire({ ...errorConfig, text: msg });
        this.loading = false;
      },
      () => (this.loading = false),
    );
  }

  getPage(page) {
    this.resolver(page);
  }

  previous() {
    this.resolver(this.page - 1);
  }

  next() {
    this.resolver(this.page + 1);
  }

  last() {
    this.resolver(this.maxPages);
  }

  first() {
    this.resolver(1);
  }

  getArrNumberPages() {
    const pagination: any = paginate(this.maxPages, this.page);

    return pagination.pages;
  }

  resolver(page) {
    if (this.searchString) {
      return this.search(this.searchString, page);
    }

    return this.getAll(page);
  }
}
