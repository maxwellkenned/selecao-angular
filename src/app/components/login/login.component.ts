import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { errorConfig, successConfig } from '../../config/swalConfig';

import { SessionService } from 'src/app/services/session.service';
import { User } from 'src/app/models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });
  constructor(
    private fb: FormBuilder,
    private sessionService: SessionService,
    private router: Router,
  ) {}

  ngOnInit() {}

  onSubmit() {
    const user: User = {
      name: '',
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };

    this.sessionService.login(user).subscribe(
      result => {
        Swal.fire({ ...successConfig, text: 'Logado com sucesso!' });

        return this.router.navigate(['home']);
      },
      error => {
        const msg = (error && error.error) || 'Ocorreu um erro!';
        Swal.fire({ ...errorConfig, text: msg });
      },
    );
  }

  getFriendlyName() {
    return {
      email: 'E-mail',
      password: 'Senha',
    };
  }
}
