import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { errorConfig, successConfig } from '../../config/swalConfig';

import { UploadService } from 'src/app/services/upload.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
})
export class UploadComponent implements OnInit {
  public uploadForm = this.fb.group({
    file: ['', Validators.required],
  });

  @ViewChild('file', { static: false }) fileInput: any;
  public file: File;

  constructor(private fb: FormBuilder, private uploadService: UploadService) {}

  ngOnInit() {}

  getFriendlyName() {
    return {
      file: 'Arquivo CSV',
    };
  }

  addFile() {
    this.file = this.fileInput.nativeElement.files[0];
  }

  onSubmit() {
    this.uploadService.uploadCsv(this.file).subscribe(
      result => {
        Swal.fire({ ...successConfig, timer: 0, text: result.message });
        const loading = document.getElementById('loading');
        loading.style.display = 'block';
      },
      error => {
        const msg = (error && error.error) || 'Ocorreu um erro!';
        Swal.fire({ ...errorConfig, text: msg });
      },
    );
  }
}
