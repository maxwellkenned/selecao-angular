import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Validators, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { errorConfig, successConfig } from '../../config/swalConfig';

import { User } from 'src/app/models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  public registerForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
  ) {}

  ngOnInit() {}

  onSubmit() {
    const user: User = {
      name: this.registerForm.value.name,
      email: this.registerForm.value.email,
      password: this.registerForm.value.password,
    };

    this.userService.createUser(user).subscribe(
      result => {
        Swal.fire({ ...successConfig, text: 'Cadastrado com sucesso!' });
        return this.router.navigate(['login']);
      },
      error => Swal.fire({ ...errorConfig, text: error.error }),
    );
  }

  getFriendlyName() {
    return {
      name: 'Nome',
      email: 'E-mail',
      password: 'Senha',
    };
  }
}
