import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private sessionService: SessionService, private router: Router) {}

  ngOnInit() {}

  logout() {
    this.sessionService.logout();
    this.router.navigate(['']);
  }
}
