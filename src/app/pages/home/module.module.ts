import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './home.component';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { SidebarComponent } from 'src/app/components/sidebar/sidebar.component';
import { MainComponent } from 'src/app/components/main/main.component';
import { UploadComponent } from 'src/app/components/upload/upload.component';
import { AvgComponent } from 'src/app/components/price-history/avg/avg.component';
import { RegionComponent } from 'src/app/components/price-history/region/region.component';
import { DistributorComponent } from 'src/app/components/price-history/distributor/distributor.component';
import { DateCollectionComponent } from 'src/app/components/price-history/date-collection/date-collection.component';
import { AvgCityComponent } from 'src/app/components/price-history/avg-city/avg-city.component';
import { AvgProviderComponent } from 'src/app/components/price-history/avg-provider/avg-provider.component';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    SidebarComponent,
    MainComponent,
    UploadComponent,
    AvgComponent,
    RegionComponent,
    DistributorComponent,
    DateCollectionComponent,
    AvgCityComponent,
    AvgProviderComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HomeRoutingModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class HomeModule {}
