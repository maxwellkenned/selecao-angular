import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { UploadComponent } from 'src/app/components/upload/upload.component';
import { LoginGuard } from 'src/app/guards/login.guard';
import { AvgComponent } from 'src/app/components/price-history/avg/avg.component';
import { RegionComponent } from 'src/app/components/price-history/region/region.component';
import { DistributorComponent } from 'src/app/components/price-history/distributor/distributor.component';
import { DateCollectionComponent } from 'src/app/components/price-history/date-collection/date-collection.component';
import { AvgCityComponent } from 'src/app/components/price-history/avg-city/avg-city.component';
import { AvgProviderComponent } from 'src/app/components/price-history/avg-provider/avg-provider.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    canActivateChild: [LoginGuard],
    children: [
      { path: '', redirectTo: 'upload', pathMatch: 'full' },
      { path: 'upload', component: UploadComponent },
      { path: 'pricehistories/avg', component: AvgComponent },
      { path: 'pricehistories/avg-city', component: AvgCityComponent },
      { path: 'pricehistories/avg-provider', component: AvgProviderComponent },
      { path: 'pricehistories/region', component: RegionComponent },
      { path: 'pricehistories/distributor', component: DistributorComponent },
      {
        path: 'pricehistories/date-collection',
        component: DateCollectionComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
