import { SweetAlertOptions } from 'sweetalert2';

const defaultConfig: SweetAlertOptions = {
  toast: true,
  position: 'top',
  timer: 0,
  timerProgressBar: true,
};

export const errorConfig: SweetAlertOptions = {
  ...defaultConfig,
  icon: 'error',
  title: 'Oops...',
};

export const successConfig: SweetAlertOptions = {
  ...defaultConfig,
  icon: 'success',
  title: 'Sucesso!',
};
