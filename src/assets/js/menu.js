function toggleMenu() {
  const sidebar = $('aside.sidebar')[0];

  if (sidebar.classList) {
    sidebar.classList.toggle('repressed');
  }
}
